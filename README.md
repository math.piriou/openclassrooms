Ce projet a pour objectif d'ajouter des plugins Ansible au module communautaire [infra-ovha-ansible-module](https://github.com/synthesio/infra-ovh-ansible-module).

Dans le cadre de mon alternance, on avait besoin d'automatiser la création d'un (puis plusieurs) serveur *public cloud* chez OVH avec une certaine configuration. 

Pour ce faire, OVH propose des APIs, le seul problème est que ces derniers ne sont pas prit en charge par Ansible. 

Ce module [infra-ovh-ansible-module](https://github.com/synthesio/infra-ovh-ansible-module), propose un plugin permettant de créer une *instance public cloud*. Il s'agit du module [public_cloud_instance](https://github.com/synthesio/infra-ovh-ansible-module/blob/master/plugins/modules/public_cloud_instance.py).

Pour mon alternance, on avait besoin que le serveur créé puisse être connecté au VRACK (réseau virtuel d'OVH) et avoir par exemple une IP Failover. Le script **public_cloud_instance_private_network.py** s'occupe d'attacher le serveur au réseau privé et **public_cloud_instance_failover_ip** de lui lier une IP Failover.

Lorsqu'on travail avec les APIs d'OVH, il faut utiliser des IDs pour aller chercher des variables spécifiques à OVH. Le script **public_cloud_failover_ip_info.py** permet d'utiliser l'IP Failover que l'on souhaite (on à une plage d'IP) sans passer par son ID qui peut être composé de 36 charactères. Voici un autre exemple de ce que ça donne avec le nom des clefs SSH à utiliser (pour qu'OVH crée le serveur) [script](https://github.com/article714/infra-ovh-ansible-module/blob/master/plugins/modules/public_cloud_ssh_key_info_by_name.py). Ce plugin est dans le fork **synthesio - infra-ovh-ansible-module** utilisé par mon entreprise, c'est presque le même code que l'IP Failover.  

Chaque script, suit la norme Ansible, c'est-à-dire qu'il y a une documentation avec un exemple à l'intérieur du script. 

Le "codage" est assez similaire pour chacun des scripts : 

Définitions des variables à utiliser 

```python
# script https://gitlab.com/math.piriou/openclassrooms/-/blob/main/public_cloud_instance_failover_ip.py

...
def run_module():
    module_args = ovh_argument_spec()
    module_args.update(
        dict(
            service_name=dict(required=True),
            fo_ip_id=dict(required=True),
            instance_id=dict(required=False),
            state=dict(choices=["present", "absent"], default="present"),
        )
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)
    client = ovh_api_connect(module)
    service_name = module.params["service_name"]
    fo_ip_id = module.params["fo_ip_id"]
    instance_id = module.params["instance_id"]
    state = module.params["state"]


...
```

Appel d'un API avec la gestion des erreurs (généralement il s'agit d'un **get** pour obtenir des informations): 

```python
# script https://gitlab.com/math.piriou/openclassrooms/-/blob/main/public_cloud_instance_failover_ip.py
...
try:
        ip_data = client.get(
            "/cloud/project/{0}/ip/failover/{1}".format(service_name, fo_ip_id)
        )
    except APIError as api_error:
        module.fail_json(
            msg="Error getting failover ip data list: {0}".format(api_error)
        )
...
```

Ensuite, en fonction du script, plusieurs actions se font. Généralement, on va tester l'une des variables, vérifier certaines conditions et peut-être même appeler d'autres APIs (pour attacher une IP par exemple).

```python
# script https://gitlab.com/math.piriou/openclassrooms/-/blob/main/public_cloud_failover_ip_info.py
...
fo_ips_list = []
    try:
        fo_ips_list = client.get("/cloud/project/{0}/ip/failover".format(service_name))
    except APIError as api_error:
        module.fail_json(msg="Error getting failover ips list: {0}".format(api_error))

    for ip_data in fo_ips_list:

        if ip_data["ip"] == fo_ip:
            module.exit_json(changed=False, **ip_data)

    module.fail_json(
        msg="Error: could not find given fail-over IP {0} in {1}".format(
            fo_ip, fo_ips_list
        )
    )

...
```


Voici un exemple de l'un des scripts utilisés dans un playbook ansible :

```yml
    - name: Get Id of SSH Key
      synthesio.ovh.public_cloud_ssh_key_info_by_name:
        #On se connecte aux APIs d'OVH  
        endpoint: "{{ OVH_ENDPOINT }}"
        application_key: "{{ OVH_APPLICATION_KEY }}"
        application_secret: "{{ OVH_APPLICATION_SECRET }}"
        consumer_key: "{{ OVH_CONSUMER_KEY }}"
        # On rentre l'ID du projet 
        service_name: "{{ project_info['project_id'] }}"
        # Vu qu'on utilise le mode pour récupérer l'ID du SSH par son nom, on utilise directement son nom
        ssh_key_name: "{{ nom_ssh_key }}"
      # On enregistre le résultat dans un register   
      register: ssh_key_id
      delegate_to: localhost
      become: no

```

Ce qui permet de créer un playbook dans ce format :

```yml
    - name: Create the Public Cloud Instance (PCI)
      synthesio.ovh.public_cloud_instance:
        endpoint: "{{ OVH_ENDPOINT }}"
        application_key: "{{ OVH_APPLICATION_KEY }}"
        application_secret: "{{ OVH_APPLICATION_SECRET }}"
        consumer_key: "{{ OVH_CONSUMER_KEY }}"
        name: "{{ pci_instance_name }}"
        #Ici on récupère le register du playbook précédent et on va chercher dans les résultats du plugin la ligne "id" qui correspond à l'id du SSH
        ssh_key_id: "{{ ssh_key_id['id'] }}"
        service_name: "{{ project_id }}"
        flavor_id: "{{ flavor_id }}"
        region: "{{ pci_region }}"
        image_id: "{{ image_id }}"
        monthly_billing: "{{ pci_monthly_billing | default(false) }}"
        force_reinstall: "{{ pci_force_reinstall | default(false) }}"
      register: instance_info
      delegate_to: localhost
      become: no
```
